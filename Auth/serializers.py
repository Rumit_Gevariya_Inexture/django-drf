from django.contrib.auth import password_validation, get_user_model
from rest_framework import serializers
from rest_framework.authtoken.models import Token
from rest_framework_simplejwt.tokens import RefreshToken, TokenError

from .managers import CustomUserManager
from .models import User


class RegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['email', 'password']

    def validate_email(self, value):
        if user := User.objects.filter(email=value):
            raise serializers.ValidationError("email is already taken")
        return CustomUserManager.normalize_email(value)


class LoginSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True)
    password = serializers.CharField(required=True)


class EmptySerializer(serializers.ModelSerializer):
    pass


class LogoutSerializer(serializers.Serializer):
    refresh = serializers.CharField(required=True)
    default_error_message = {
        'bad_token': 'Token is expired or invalid'
    }

    def save(self, **kwargs):
        print(kwargs)
        try:
            RefreshToken().blacklist()
        except TokenError:
            self.fail('bad_token')


class PasswordChangeSerializer(serializers.Serializer):
    current_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)

    def validate_current_password(self, value):
        if not self.context['request'].user.check_password(value):
            raise serializers.ValidationError('Current password does not match')
        return value

    def validate_new_password(self, value):
        password_validation.validate_password(value)
        return value


class PasswordResetSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True)


class TokenSerializer(serializers.ModelSerializer):
    token = serializers.SerializerMethodField()

    class Meta:
        model = get_user_model()
        fields = ('token',)

    def get_token(self, obj):
        token = Token.objects.get(user=obj)
        return token.key if token is not None else ''
