from django.contrib.auth import authenticate, get_user_model
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from rest_framework import serializers
from rest_framework_simplejwt.tokens import RefreshToken


def create_user_account(email, password, first_name="", last_name="", **extra_fields):
    return get_user_model().objects.create_user(email=email, password=password, first_name=first_name, last_name=last_name, **extra_fields)


def get_and_authenticate_user(email, password):
    user = authenticate(email=email, password=password)
    if user is None:
        raise serializers.ValidationError("Invalid email/password. Please try again!")
    # ensure to have a valid token
    # Token.objects.get_or_create(user=user)
    return get_tokens_for_user(user)


def get_user_by_email(email):
    return get_user_model().objects.filter(email__iexact=email).first()


def get_token_for_password_reset(user):
    user_pk = urlsafe_base64_encode(force_bytes(user.pk))
    reset_token = PasswordResetTokenGenerator().make_token(user)
    return f"{user_pk}::{reset_token}"


def send_password_reset_mail(user):
    token = get_token_for_password_reset(user)
    print(token)
    print(f'TODO: SENT EMAIL TO USER WITH CONFIRMATION CODE: {token}')
    return ''


# Generate Token Manually
def get_tokens_for_user(user):
  refresh = RefreshToken.for_user(user)
  return {
      'refresh': str(refresh),
      'access': str(refresh.access_token),
  }
