from django.contrib.auth import get_user_model
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase


class AuthLoginViewTests(APITestCase):
    def setUp(self):
        self.email1 = "u1@domain.in"
        self.email2 = "u2@domain.in"
        self.password1 = "b050ce57eb2081d4423640a12a1351d7"
        self.password2 = "0db32b655c04c67bf7084de29e1c9676"

        get_user_model().objects.create_user(
            email=self.email1,
            password=self.password1,
        )
        get_user_model().objects.create_user(
            email=self.email2,
            password=self.password2,
        )

    def test_invalid(self):
        payload = None
        response = self.client.post(reverse("auth-login"), data=payload)
        self.assertNotEqual(response.status_code, status.HTTP_200_OK)

        payload = {}
        response = self.client.post(reverse("auth-login"), data=payload)
        self.assertNotEqual(response.status_code, status.HTTP_200_OK)

        payload = {
            "unknown": "value"
        }
        response = self.client.post(reverse("auth-login"), data=payload)
        self.assertNotEqual(response.status_code, status.HTTP_200_OK)

        payload = {
            "email": "wrong"
        }
        response = self.client.post(reverse("auth-login"), data=payload)
        self.assertNotEqual(response.status_code, status.HTTP_200_OK)

        payload = {
            "email": "u1@domain.io"
        }
        response = self.client.post(reverse("auth-login"), data=payload)
        self.assertNotEqual(response.status_code, status.HTTP_200_OK)

        payload = {
            "email": "u1@domain.io",
            "password": ""
        }
        response = self.client.post(reverse("auth-login"), data=payload)
        self.assertNotEqual(response.status_code, status.HTTP_200_OK)

        payload = {
            "email": "u1@domain.io",
            "password": "wrongpassword"
        }
        response = self.client.post(reverse("auth-login"), data=payload)
        self.assertNotEqual(response.status_code, status.HTTP_200_OK)

    def test_signal(self):
        payload = {
            "email": self.email1,
            "password": self.password1
        }
        response = self.client.post(reverse("auth-login"), data=payload)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(response.data is not None)
        self.assertTrue(response.data["refresh"] is not None)
        self.assertTrue(response.data["access"] is not None)
