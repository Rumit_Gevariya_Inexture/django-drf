from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from Auth.models import User, UserProfile


class AuthRegisterViewTests(APITestCase):
    def setUp(self):
        self.email = "001@tld.com"
        self.password = "1234qwer"

    def test_invalid(self):
        payload = None
        response = self.client.post(reverse("auth-register"), data=payload)
        self.assertNotEqual(response.status_code, status.HTTP_201_CREATED)

        payload = {}
        response = self.client.post(reverse("auth-register"), data=payload)
        self.assertNotEqual(response.status_code, status.HTTP_201_CREATED)

        payload = {
            "unknown": "value"
        }
        response = self.client.post(reverse("auth-register"), data=payload)
        self.assertNotEqual(response.status_code, status.HTTP_201_CREATED)

        payload = {
            "email": "wrong"
        }
        response = self.client.post(reverse("auth-register"), data=payload)
        self.assertNotEqual(response.status_code, status.HTTP_201_CREATED)

        payload = {
            "email": "right@domain.io"
        }
        response = self.client.post(reverse("auth-register"), data=payload)
        self.assertNotEqual(response.status_code, status.HTTP_201_CREATED)

        payload = {
            "email": "right@domain.io",
            "password": ""
        }
        response = self.client.post(reverse("auth-register"), data=payload)
        self.assertNotEqual(response.status_code, status.HTTP_201_CREATED)

    def test_signal(self):
        payload = {
            "email": self.email,
            "password": self.password
        }
        response = self.client.post(reverse("auth-register"), data=payload)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_duplicate(self):
        payload = {
            "email": self.email,
            "password": self.password,
        }
        response = self.client.post(reverse("auth-register"), data=payload)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response = self.client.post(reverse("auth-register"), data=payload)
        self.assertEqual(response.data['email'][0], "user with this email_address already exists.")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_ensure_user_profile_created_and_deleted_automatically(self):
        payload = {
            "email": self.email,
            "password": self.password,
        }
        response = self.client.post(reverse("auth-register"), data=payload)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # user_profile created
        user = User.objects.filter(email=payload["email"]).first()
        profile = UserProfile.objects.filter(user=user).first()
        if not user:
            self.fail("user_profile was not created")
        self.assertTrue(profile is not None)
        self.assertTrue(profile.pk > 0)

        # user_profile deleted
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(UserProfile.objects.count(), 1)
        user.delete()
        self.assertEqual(User.objects.count(), 0)
        self.assertEqual(UserProfile.objects.count(), 0)
