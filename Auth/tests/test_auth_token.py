from django.contrib.auth import get_user_model
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient
from rest_framework.test import APITestCase


class AuthTokenViewTests(APITestCase):
    def setUp(self):
        self.email1 = "u1@domain.in"
        self.password1 = "b050ce57eb2081d4423640a12a1351d7"

        get_user_model().objects.create_user(
            email=self.email1,
            password=self.password1,
        )

    def test_user_logout_login_new_token(self):
        payload = {
            "email": self.email1,
            "password": self.password1,
        }

        # login -> create new token
        response = self.client.post(reverse("auth-login"), data=payload)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(response.data["access"] is not None)
        self.assertTrue(response.data["refresh"] is not None)

        # update client
        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {response.data["access"]}')

        # logout -> remove token
        payload_for_logout = {
            "refresh": response.data["refresh"]
        }
        response = self.client.post(reverse("auth-logout"), data=payload_for_logout)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        # update client
        self.client = APIClient()

        # re-login -> re-create new token
        response = self.client.post(reverse("auth-login"), data=payload)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(response.data["access"] is not None)
        self.assertTrue(response.data["refresh"] is not None)
