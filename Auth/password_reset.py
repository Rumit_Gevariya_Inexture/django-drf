from django_rest_passwordreset.views import ResetPasswordRequestToken
from django_rest_passwordreset.views import (
    ResetPasswordRequestTokenViewSet, )
from rest_framework import status
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet


class ResetPasswordRequestTokenViewSet(ResetPasswordRequestToken, GenericViewSet):
    """
    An Api ViewSet which provides a method to request a password reset token based on an e-mail address

    Sends a signal reset_password_token_created when a reset token was created
    """

    def create(self, request, *args, **kwargs):
        super(ResetPasswordRequestTokenViewSet, self).post(request, *args, **kwargs)
        return Response(data={"status": "password reset link send on your email."}, status=status.HTTP_201_CREATED)
