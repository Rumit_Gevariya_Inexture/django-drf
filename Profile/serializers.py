from django.contrib.auth import get_user_model
from rest_framework import serializers

from .models import UserProfile

User = get_user_model()


class UserSerializers(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ('email', 'first_name', 'last_name')


class UserProfileSerializer(serializers.ModelSerializer):
    # associations
    user = UserSerializers(many=False, read_only=True)

    class Meta:
        model = UserProfile
        fields = ['uuid', 'user', 'phone', 'profile_image', 'profession', ]


class ProfileUpdateSerializer(serializers.ModelSerializer):
    # add the serializer for the foreignkey model
    first_name = serializers.CharField(max_length=100, required=False)
    last_name = serializers.CharField(max_length=100, required=False)
    email = serializers.EmailField(required=False)

    class Meta:
        model = UserProfile
        fields = ['phone', 'profile_image', 'profession', 'first_name', 'last_name', 'email']  #

    def update(self, instance, validated_data):
        # Unless the application properly enforces that this field is
        # always set, the following could raise a `DoesNotExist`, which
        # would need to be handled.
        user = instance.user

        instance.phone = validated_data.get('phone', instance.phone)
        instance.profession = validated_data.get('profession', instance.profession)
        instance.profile_image = validated_data.get('profile_image', instance.profile_image)
        instance.save()

        user.first_name = validated_data.get('first_name', user.first_name)
        user.last_name = validated_data.get('last_name', user.last_name)
        user.email = validated_data.get('email', user.email)
        user.save()
        return instance
