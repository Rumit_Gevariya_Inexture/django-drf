from django.urls import reverse
from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.test import APITestCase, APIClient


class UserProfileViewTests(APITestCase):
    def setUp(self):
        self.email1 = "u1@domain.in"
        self.password1 = "b050ce57eb2081d4423640a12a1351d7"

        get_user_model().objects.create_user(
            email=self.email1,
            password=self.password1,
        )

    def test_user_profile(self):
        # login -> user authentication
        payload = {
            "email": self.email1,
            "password": self.password1,
		}
        response = self.client.post(reverse("auth-login"), data=payload)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(response.data is not None)
        self.assertTrue(response.data["refresh"] is not None)
        self.assertTrue(response.data["access"] is not None)

        self.access_token = response.data["access"]
        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.access_token}')
        response = self.client.get(reverse("user-profile"), data=None)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(response.data is not None)

        # check the profile update api.
        payload = {
            "phone": "+41524204242",
            "profession": "test profression",
            "first_name": "test",
            "last_name": "account",
            "email": "test.account@gmail.com"
        }
        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.access_token}')
        response = self.client.patch(reverse("user-profile"), data=payload)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["data"], "profile updated successfully.")