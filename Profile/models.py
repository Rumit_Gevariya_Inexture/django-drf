import uuid

from Base.models import BaseTable
from django.conf import settings
from django.db import models
from phonenumber_field.modelfields import PhoneNumberField


class UserProfile(BaseTable):
    """The User Profile Model."""

    uuid = models.UUIDField(unique=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, blank=True, null=True, )
    phone = PhoneNumberField(null=False, blank=True, )
    profile_image = models.FileField(upload_to="user/profiles", default='default.jpg')
    profession = models.CharField(max_length=100, blank=True)

    def __str__(self):
        return self.uuid

    class Meta:
        db_table = 'user_profile'
        ordering = ['-id']
