from django.http import Http404
from oauth2_provider.contrib.rest_framework import authentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView, status
from rest_framework_simplejwt.authentication import JWTAuthentication

from .models import UserProfile
from .serializers import UserProfileSerializer, ProfileUpdateSerializer


# Create your views here.
class UserProfileView(APIView):
    """
        List all users profiles.
    """
    authentication_classes = [authentication.OAuth2Authentication, JWTAuthentication]
    permission_classes = [IsAuthenticated]

    def get_object(self, user):
        try:
            return UserProfile.objects.get(user=user)
        except UserProfile.DoesNotExist:
            raise Http404

    def get(self, request):
        profile_obj = self.get_object(request.user)
        serializer = UserProfileSerializer(profile_obj)
        return Response(serializer.data)

    def patch(self, request):
        profile_obj = self.get_object(request.user)
        serializer = ProfileUpdateSerializer(profile_obj, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(data={"data": "profile updated successfully."}, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
