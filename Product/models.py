import uuid

from django.conf import settings
from django.db import models

from Base.models import BaseTable


# Create your models here.
class Product(BaseTable):
    """The Product Model."""

    uuid = models.UUIDField(unique=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, blank=True, null=True, )
    name = models.CharField(max_length=200)
    discription = models.TextField()
    price = models.FloatField()
    quantity = models.IntegerField()
    product_image = models.FileField(upload_to="products", )

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'product'
        ordering = ['-id']
