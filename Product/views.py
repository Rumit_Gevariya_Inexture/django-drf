from django.http import Http404
from django_filters import FilterSet
from django_filters import rest_framework as filters
from django_filters.rest_framework import DjangoFilterBackend
from oauth2_provider.contrib.rest_framework import OAuth2Authentication
from rest_framework import generics
from rest_framework import status
from rest_framework.filters import OrderingFilter, SearchFilter
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.authentication import JWTAuthentication

from .models import Product
from .serializers import ProductSerializer, ProductsSerializer


# Create your views here.


class ProductList(APIView):
    """
        List all product, or create a new product.
        """
    authentication_classes = [OAuth2Authentication, JWTAuthentication]
    permission_classes = [IsAuthenticated, ]

    def get(self, request, ):
        products = Product.objects.all()
        serializer = ProductSerializer(products, many=True)
        return Response(serializer.data)

    def post(self, request, ):
        serializer = ProductSerializer(data=request.data, context={"request": request})
        serializer.is_valid(raise_exception=True)
        instance = serializer.save()
        data = {
            "msg": "Product Created Successfully.",
            "product id": instance.uuid,
            "product title": instance.name
        }
        return Response(data=data, status=status.HTTP_201_CREATED)


class ProductDetail(APIView):
    """
    Retrieve, update or delete a snippet instance.
    """
    authentication_classes = [OAuth2Authentication, JWTAuthentication]
    permission_classes = [IsAuthenticated, ]

    def get_object(self, product_id, user):
        try:
            return Product.objects.filter(uuid=product_id, user=user).first()
        except Product.DoesNotExist:
            raise Http404

    def get(self, request, product_id):
        if product := self.get_object(product_id, request.user):
            serializer = ProductSerializer(product)
            return Response(serializer.data)
        else:
            return Response(data={"msg": "Product Not Found."})

    def patch(self, request, product_id, ):
        if product := self.get_object(product_id, request.user):
            serializer = ProductSerializer(product, data=request.data, partial=True)
            if serializer.is_valid():
                serializer.save()
                data = {
                    "msg": "product data updated successfully.",
                }
                return Response(data=data, status=status.HTTP_200_OK)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(data={"msg": "Product Not Found."})

    def delete(self, request, product_id, ):
        if product := self.get_object(product_id, request.user):
            product.delete()
            return Response(data={"msg": "Product Deleted Successfully."}, status=status.HTTP_204_NO_CONTENT)
        else:
            return Response(data={"msg": "Product Not Found."})


# class GetProduct(APIView):
#     """List all products.
#             """
#     permission_classes = [AllowAny, ]
#     filter_backends = (DjangoFilterBackend,)
#     filter_fields = ('name', 'user__is_active',)

#     def get(self, request):
#         products = Product.objects.all()
#         serializer = ProductsSerializer(products, many=True)
#         return Response(serializer.data)

# custom filter.
class ProductFilter(FilterSet):
    product_n = filters.CharFilter('name')
    user = filters.CharFilter("user__email")
    dis = filters.CharFilter("discription")

    class Meta:
        model = Product
        fields = ("product_n", "user", "dis")


class GetProduct(generics.ListAPIView):
    serializer_class = ProductsSerializer
    queryset = Product.objects.all()
    filter_backends = (DjangoFilterBackend, OrderingFilter, SearchFilter,)
    filter_class = ProductFilter
    ordering_fields = ('name',)
    ordering = ('name',)
    search_fields = ('name',)
