from django.urls import path

from .views import ProductList, ProductDetail, GetProduct

urlpatterns = [
    path('', ProductList.as_view()),
    path('<str:product_id>/', ProductDetail.as_view()),
    path('products/all/', GetProduct.as_view()),
]
