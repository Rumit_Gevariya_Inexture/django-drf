from rest_framework import serializers

from Profile.models import UserProfile
from Profile.serializers import UserProfileSerializer
from Profile.serializers import UserSerializers
from .models import Product


class ProductSerializer(serializers.ModelSerializer):
    # user = UserSerializers(read_only=True)
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())
    user_data = serializers.SerializerMethodField('get_user_data')

    class Meta:
        model = Product
        fields = ["uuid", "name", "discription", "price", "quantity", "product_image", "user",
                  "created_on",
                  "modified_on", "user_data"]

    def get_user_data(self, product_object):
        profile_instance = UserProfile.objects.filter(user=product_object.user).first()
        return UserProfileSerializer(profile_instance, many=False).data


class ProductsSerializer(serializers.ModelSerializer):
    user = UserSerializers()
    user_email = serializers.CharField(source='user.email')

    class Meta:
        model = Product
        fields = ["uuid", "name", "user_email", "discription", "price", "quantity", "product_image", "created_on",
                  "modified_on", "user"]
        read_only_fields = ['user']
