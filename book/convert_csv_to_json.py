import csv
import json

with open('fixtures/book_title_author2v2.csv', 'r') as input_file:
    reader = csv.DictReader(input_file)

    jsonoutput = 'fixtures/book_title_author.json'
    with open(jsonoutput, 'a') as output_file:
        for row in reader:
            json.dump(row, output_file)
            output_file.write('\n')
