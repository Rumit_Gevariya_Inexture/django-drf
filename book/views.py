import json
import os

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
from django.shortcuts import HttpResponse
from rest_framework import status
from rest_framework.decorators import APIView
from rest_framework.response import Response

from Auth.models import User
from .models import Post
from .serializers import PostSerializer


# Create your views here.
def insert_json_data(request):
    module_dir = os.path.dirname(__file__)  # get current directory
    file_path = os.path.join(module_dir, 'post.json')
    with open(file_path, encoding='utf-8') as data_file:
        json_data = json.loads(data_file.read())
        for post_data in json_data:
            user = User.objects.get(id=post_data["user_id"])
            post_data.update({"user_id": user})
            post = Post.objects.create(**post_data)
            # posts created
    return HttpResponse("Done Data-insert Process.")


# get all products.
class PostView(APIView):
    def get(self, request, *args, **kwargs):
        page = request.GET.get('page', 1)
        limit = request.GET.get('limit', 10)
        try:
            posts = Post.objects
            if request.GET.get("title"):
                title = request.GET.get("title")
                posts = posts.filter(Q(title__contains=title) | Q(content__contains=title))
            else:
                posts = posts.all()
            if request.GET.get("sort_by"):
                sort_by = request.GET.get("sort_by")
                if sort_by == 'DESC':  # High TO Low
                    posts = posts.order_by('-date_posted')
                elif sort_by == 'ASC':  # Low To High
                    posts = posts.order_by('date_posted')
                else:
                    posts = posts.all()
            else:
                posts = posts.all()
            paginator = Paginator(posts, limit)
            try:
                posts_list = paginator.page(page)
            except PageNotAnInteger:
                return Response(data={'error': True,
                                      "code": status.HTTP_400_BAD_REQUEST,
                                      "msg": "Page not found."}, status=status.HTTP_400_BAD_REQUEST)
            except EmptyPage:
                return Response(data={'error': True,
                                      "code": status.HTTP_400_BAD_REQUEST,
                                      "msg": "Page not found."}, status=status.HTTP_400_BAD_REQUEST)
            serializer = PostSerializer(posts_list, many=True)
            data = {
                "page": page,
                "total_page": paginator.num_pages,
                "total_records": paginator.count,
                "page_limit": limit,
                "has_prev": posts_list.has_previous(),
                "has_next": posts_list.has_next(),
                'data': serializer.data,
            }
            return Response(data={'error': False,
                                  "code": status.HTTP_200_OK,
                                  "data": data})
        except Exception as e:
            return Response(data={'error': True,
                                  "code": status.HTTP_400_BAD_REQUEST,
                                  "msg": "Something want wrong."}, status=status.HTTP_400_BAD_REQUEST)
