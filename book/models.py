from django.conf import settings
from django.db import models


# Create your models here.
class Book(models.Model):
    title = models.CharField(max_length=1000, null=False, db_index=True)
    authors = models.CharField(max_length=1000)


class Post(models.Model):
    title = models.CharField(max_length=100, null=False)
    date_posted = models.DateTimeField(auto_now_add=True)
    content = models.TextField(null=False)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, blank=True, null=True, )
