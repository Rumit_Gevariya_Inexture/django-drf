from django.urls import path, re_path

from .views import PostView
from .views import insert_json_data

urlpatterns = [
    path("insert/data/", insert_json_data),
    re_path(r"(?:params)?$", PostView.as_view(), name='posts'),
]
