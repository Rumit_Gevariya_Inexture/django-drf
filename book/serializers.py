from rest_framework import serializers

from Profile.models import UserProfile
from Profile.serializers import UserProfileSerializer
from .models import Post


class PostSerializer(serializers.ModelSerializer):
    user_data = serializers.SerializerMethodField('get_user_data')

    # user = UserSerializers(read_only=True)

    class Meta:
        model = Post
        fields = ['title', 'date_posted', 'content', 'user_data']

    def get_user_data(self, book_object):
        profile_instance = UserProfile.objects.filter(user=book_object.user).first()
        return UserProfileSerializer(profile_instance, many=False).data
