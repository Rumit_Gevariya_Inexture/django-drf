"""drf_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from django_rest_passwordreset.views import (
    ResetPasswordValidateTokenViewSet,
    ResetPasswordConfirmViewSet,
)
from rest_framework import routers
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

from Auth.password_reset import ResetPasswordRequestTokenViewSet
from Auth.views import AuthViewSet

# Project routes
router = routers.DefaultRouter(trailing_slash=False)
router.register('', AuthViewSet, basename='auth')
router.register('passwordreset/validate_token', ResetPasswordValidateTokenViewSet, basename='reset-password-validate')
router.register('passwordreset/confirm', ResetPasswordConfirmViewSet, basename='reset-password-confirm')
router.register('passwordreset/', ResetPasswordRequestTokenViewSet, basename='reset-password-request')

urlpatterns = [
                  path('admin/', admin.site.urls),
                  path('api/auth/', include(router.urls)),
                  path('user/', include("Profile.urls")),
                  path('product/', include("Product.urls")),
                  path('base/', include("Base.urls")),
                  path('book/', include('book.urls')),

                  # jwt token apis.
                  path('auth/login/', TokenObtainPairView.as_view(), name='login'),
                  path('auth/refresh-token/', TokenRefreshView.as_view(), name='refreshtoken'),

                  path('auth/', include('drf_social_oauth2.urls', namespace='drf')),

              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
